# unifi-slim-docker-image

This repository contains the build steps for a [unifi controller](https://help.ui.com/hc/en-us/articles/220066768).

It differs from other projects like [linuxserver/unifi-controller](https://hub.docker.com/r/linuxserver/unifi-controller) by providing a much more slimmed down (non-init based) image; expecting dependencies like mongodb to come from separate containers in a k8s or docker-compose like environment rather than being all bundled into one image.

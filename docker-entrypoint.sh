#!/bin/sh

if [ -f /app/data/system.properties ]; then
  # sort -kXn will somehow ignore all other fields for the purposes of unique
  # so we can use this to say "take the first match of a.b="
  # so if our system.properties comes FIRST then it'll beat anything in the existing file
  # and finally write out to the existing file
  sort /app/system.properties /app/data/system.properties -t= -k1,1 -u | tee /app/data/system.properties.tmp
  mv /app/data/system.properties.tmp  /app/data/system.properties
else
  # previous versions don't exist, just copy over ours in its entirety
  cp /app/system.properties /app/data/system.properties
fi

/usr/bin/java -jar lib/ace.jar $@

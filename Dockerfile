# We don't care that the builder image is a bit big / we aren't doing best practices
# on running things in one RUN
# because the builder isn't what gets deployed
# strive for simplicity, (build) cachability, and debugability
#
# https://help.ui.com/hc/en-us/articles/220066768-UniFi-Network-Updating-Third-Party-non-Console-UniFi-Network-Applications-Linux-Advanced-
FROM ubuntu AS builder
RUN apt-get update && apt-get install -y gnupg ca-certificates binutils xz-utils libsystemd0
RUN echo 'deb https://www.ui.com/downloads/unifi/debian stable ubiquiti' | tee /etc/apt/sources.list.d/100-ubnt-unifi.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv 06E85760C0A52C50 
WORKDIR /tmp/unifi
RUN apt-get update && apt-get download unifi
RUN ar vx unifi*.deb
RUN tar xf data.tar.xz

FROM alpine
WORKDIR /app/
RUN apk add openjdk17-jre-headless
# unifi appears inflexible when it comes to log location
# so let's just pretend the log file is /dev/stdout instead
RUN mkdir /app/logs && ln -sf /dev/stdout /app/logs/server.log

EXPOSE 8080 8443 8843 8880
VOLUME /app/data

COPY docker-entrypoint.sh /app/
ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["start"]

# COPY from multistage is always last because it seems to be uncachable
COPY --from=builder /tmp/unifi/usr/lib/unifi /app/
